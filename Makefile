DOT   ?= 'timeline.dot'
PNG   ?= 'timeline.png'
PRCNT := %
DAYS  ?= 15
START ?= $$(date -d "now - $$(($(DAYS)-5)) days" +$(PRCNT)F)

all: chk dot qiv

chk:
	shellcheck timeline2dot

dot:
	START=$(START) DAYS=$(DAYS) ./timeline2dot > $(DOT)
	dot -Tpng $(DOT) > $(PNG)

qiv:
	qiv -fi $(PNG)
